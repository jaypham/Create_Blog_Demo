CreateBlog::Application.routes.draw do

  resources :import_files
  devise_for :users, :controllers => {registrations: "registrations"}

  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  resources :informations

  resources :posts do
  resources :comments
  end
  root "posts#index"
  get '/about', to: 'pages#about'

  resources :informations do
    collection { post :import }
  end

  get 'update', to: 'informations#update'
end
