class InformationsController < ApplicationController

  def index
    @informations = Information.all # find(params[:id])
    #respond_to do |format|
    #format.html
    #format.csv {send_data @informations.to_csv}
    #end
  end

  def import
    begin
    Information.import(params[:file])
    redirect_to informations_path, notice: "Data Records Imported!"
    rescue
    redirect_to informations_path, notice: "Error! Try Again"
    end
    end
end

def show
  @informations = Information.find(params[:id])
end

def new
  @informations = current_user.informations.build
end

def edit
  @informations = Information.all
end

def update
  if @informations.update(params[:information].permit(:id, :name, :email, :status))
    redirect_to @informations
  else
    render 'edit'
  end

  def destroy
    @informations.destroy
    redirect_to informations_path
  end
  private

  def information_params
    params.require(:information).permit(:id, :name, :email, :status)
  end
end