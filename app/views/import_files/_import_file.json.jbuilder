json.extract! import_file, :id, :user_id, :name, :email, :status, :created_at, :updated_at
json.url import_file_url(import_file, format: :json)
