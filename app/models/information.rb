  class Information < ActiveRecord::Base
    require 'csv'
    def self.import(file)
      CSV.foreach(file.path, headers: true) do |row|

        information_hash = row.to_hash # exclude the price field
        information = Information.where(id:information_hash["id"])

        if information.count == 1
          information.first.update_attributes(information_hash)
        else
          Information.create!(information_hash)
        end # end if !product.nil?
      end # end CSV.foreach
    end # end self.import(file)
  end # end class

