class CreateImportFiles < ActiveRecord::Migration[5.1]
  def change
    create_table :import_files do |t|
      t.integer :user_id
      t.string :name
      t.string :email
      t.string :status

      t.timestamps
    end
  end
end
